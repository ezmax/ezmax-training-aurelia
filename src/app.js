import 'babel-polyfill';
import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';

import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';

import appRouter from './router';
import {StateService} from './services/state-service';

@inject(Router, HttpClient, StateService, EventAggregator)
export class App {
    constructor(router, http, stateSvc, eventAggregator) {
        this.router = router;
        this.stateSvc = stateSvc;
        this.ea = eventAggregator;
        this.router.configure(appRouter);
        http.configure(config => {
            config
            //.useStandardConfiguration()
                .withBaseUrl('http://localhost:8000')
                .withDefaults({
                    mode: 'cors',
                    headers: {
                        'Accept': 'application/json'
                    }
                })
                .withInterceptor({
                    request(request) {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request; // you can return a modified Request, or you can short-circuit the request by returning a Response
                    },
                    response(response) {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response; // you can return a modified Response
                    }
                });
        });
        this.http = http;

        let _this = this;
        this.myModel = {"1":"a", "2":"b"};
        function objectTracking () {
            return new Observable(observer => {
                const handler = (obj, kv) => {
                    observer.next({ obj: obj, kv: kv });
                }
                console.log(_this);
                $(_this.myModel).bind(`change`, handler);

                return () =>  {
                    $(_this.myModel).unbind(`change`, handler);
                }
            })
        }
        objectTracking().subscribe({
            next({obj, kv}) {
                console.log(obj);
                console.log(kv);
            },
            error(err) { console.log(`Error: ${ err }`) },
            complete() { console.log(`Done!`) }
        });

        this.currentTime = 0;
        window.setInterval(() => this.showTime(), 1000);
    }

    bind() {
        console.log('Hidden value from bind: ' + $('#Hidden').val());
    }

    attached() {
        this.messageReceivedSubscription = this.ea.subscribe('new-message', message => {
            message.to = 'app';
            console.log(message);
        });

        console.log('Hidden value from attached : ' + $('#Hidden').val());

        //ES5
        /*var _this = this
        $('#exButton').click(function(event){
            _this.sendData();
        })*/
        //ES6
        $('#exButton').click((event) =>{
            console.log(event);
            this.sendData();
        })
    }

    detached() {
        this.messageReceivedSubscription.dispose();
    }

    sendMsg() {
        this.ea.publish('new-message', {'msg': 'toto', 'from': 'app'});
    }

    showTime(){
        this.currentTime++;
    }

    refresh() {
        this.stateSvc.setGreeting("Hello world! " + Math.random());
    }

    appFunction() {
        console.log("app function");
    }

    sendData() {
        console.log("sendData");
    }

    update() {
        this.myModel["1"] = "c";
        $(this.myModel).trigger('change', [{"1":"c"}]);
    }
}
