export default function(config){
    config.title = 'Aurelia';
    config.options.pushState = true;
    config.options.root = '/';

    config.map([
        { route: '',          moduleId: './welcome',     nav: true, title:'Welcome' },
        { route: 'welcome',     moduleId: './welcome',       nav: false, title:'Welcome'},
        { route: 'about',     moduleId: './about',       nav: true, title:'About'}
    ]);
}