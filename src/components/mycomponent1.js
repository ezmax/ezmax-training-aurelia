import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {bindable} from 'aurelia-framework';

@inject(EventAggregator)
export class mycomponent1 {
    constructor(eventAggregator) {
        this.ea = eventAggregator;
        this.messages = [];
    }

    @bindable greeting1;

    bind(bindingContext, overrideContext) {
        this.parent = bindingContext;
    }

    attached(){
        this.messageReceivedSubscription = this.ea.subscribe('new-message', message => {
            message.to = 'mycomponent1';
            console.log(message);
        });
    }

    detached() {
        this.messageReceivedSubscription.dispose();
    }

    sendMsg() {
        this.ea.publish('new-message', {'msg':'toto', 'from':'mycomponent1'});
    }

    refresh() {
        this.parent.refresh();
    }
}