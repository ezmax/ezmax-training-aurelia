import {inject} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {bindable} from 'aurelia-framework';

@inject(EventAggregator)
export class mycomponent2 {
    constructor(eventAggregator) {
        this.ea = eventAggregator;
        this.messages = [];
    }

    @bindable greeting2;

    bind(bindingContext, overrideContext) {
        this.parent = bindingContext;
    }

    attached(){
        this.messageReceivedSubscription = this.ea.subscribe('new-message', message => {
            message.to = 'mycomponent2';
            console.log(message);
        });
    }

    detached() {
        this.messageReceivedSubscription.dispose();
    }

    sendMsg() {
        this.ea.publish('new-message', {'msg':'toto', 'from':'mycomponent2'});
    }

    refresh() {
        this.parent.refresh();
    }
}