import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {Parent} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {bindable} from 'aurelia-framework';
import {App} from 'app';

@inject(Parent.of(App), HttpClient, EventAggregator)
export class Welcome {
    constructor(app, http, eventAggregator) {
        this.parent = app;
        this.ea = eventAggregator;

        http.configure(config => {
            config
            //.useStandardConfiguration()
                .withBaseUrl('http://localhost:8000')
                .withDefaults({
                    mode: 'cors',
                    headers: {
                        'Accept': 'application/json'
                    }
                })
                .withInterceptor({
                    request(request) {
                        console.log(`Requesting ${request.method} ${request.url}`);
                        return request; // you can return a modified Request, or you can short-circuit the request by returning a Response
                    },
                    response(response) {
                        console.log(`Received ${response.status} ${response.url}`);
                        return response; // you can return a modified Response
                    }
                });
        });
        this.http = http;

        this.items = [];
        this.http.fetch('/api/aurelia')
            .then(response => response.json())
            .then(data => {
                //console.log(data);
                this.items = data;
            });
    }

    attached() {
        this.messageReceivedSubscription = this.ea.subscribe('new-message', message => {
            message.to = 'welcome';
            console.log(message);
        });
    }

    detached() {
        this.messageReceivedSubscription.dispose();
    }

    add(){
        this.items.push(
            {
                name: $("#name").val(),
                desc: $("#desc").val()
            }
        );
    }

    refresh() {
        this.parent.refresh();
    }

    sendMsg() {
        this.ea.publish('new-message', {'msg': 'toto', 'from': 'welcome'});
    }
}