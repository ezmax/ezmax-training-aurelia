//import {I18N} from 'aurelia-i18n';

export class StateService {
    //static inject = [I18N];
    /*constructor(i18n) {
        this.i18n = i18n;
        this.greeting = this.i18n.tr('GREETING');
    }*/
    constructor() {
        this.greeting = "Bonjour le monde!";
    }

    getGreeting() {
        return this.greeting;
    }

    setGreeting(greeting) {
        this.greeting = greeting;
    }
}