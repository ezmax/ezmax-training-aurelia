import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-authentication';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Endpoint} from 'aurelia-api';
import {fetch} from 'whatwg-fetch';
import {HttpClient} from 'aurelia-fetch-client';

@inject(AuthService, EventAggregator, HttpClient, Endpoint.of('api'), Endpoint.of('auth'))
export class AuthorizationService {
    constructor(authService, eventAggretator, http, apiEndpoint, authEndpoint) {
        var _this = this;

        this.authService = authService;
        this.ea = eventAggretator;
        this.http = http;
        this.apiEndpoint = apiEndpoint;
        this.authEndpoint = authEndpoint;
        this.user = null;

        //this.getUsernameFromSub();
        //this.getUser();

    }

    getUser() {
        var _this = this;
        /*var _this = this;
        this.authService.getMe()
            .then(profile => {
                _this.user = profile;
                return _this.user;
            });*/
        return new Promise(function(resolve, reject) {
            //if(this.authService.getTokenPayload()) {
            if(_this.authService.authenticated) {
                _this.authService.getMe()
                    .then(profile => {
                        //_this.user = profile;
                        resolve(profile);
                    });
            }
        });
    }

    getUserFromSub() {
        return new Promise(function(resolve, reject) {
            //if(this.authService.getTokenPayload()) {
            if(this.authService.authenticated) {
                _this.authEndpoint.find('/auth/me')
                    .then(response => {
                        _this.user = response;
                        resolve(response);
                    })
                    .catch(error => {
                        console.error(error);
                        reject(error);
                    });
            }
        });
    }
}