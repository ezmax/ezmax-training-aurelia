import {inject} from 'aurelia-framework';
import {Parent} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {bindable} from 'aurelia-framework';
import {App} from 'app';

@inject(Parent.of(App), EventAggregator)
export class About {
    constructor(app, eventAggregator) {
        this.parent = app;
        this.ea = eventAggregator;
    }

    attached() {
        this.messageReceivedSubscription = this.ea.subscribe('new-message', message => {
            message.to = 'about';
            console.log(message);
        });
    }

    detached() {
        this.messageReceivedSubscription.dispose();
    }

    refresh() {
        console.log('ici');
        this.parent.refresh();
    }

    sendMsg() {
        this.ea.publish('new-message', {'msg': 'toto', 'from': 'about'});
    }

    aboutFunction() {
        console.log("about function");
    }
}